<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvantageFusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE SCHEMA IF NOT EXISTS "dashboard"');

        Schema::create('dashboard.advantage_fus', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('advantage_id');
            $table->string('name');
            $table->text('file');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('advantage_id')->references('id')->on('dashboard.advantages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard.advantage_fus');
    }
}
