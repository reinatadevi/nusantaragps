<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE SCHEMA IF NOT EXISTS "dashboard"');

        Schema::create('dashboard.sites', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('admin_id');
            $table->string('url');
            $table->string('email')->unique();
            $table->string('telephone');
            $table->string('address');
            $table->string('postal_code');
            $table->string('description');
            $table->string('author');
            $table->string('copyright');
            $table->text('about');
            $table->timestamps();

            $table->foreign('admin_id')->references('id')->on('dashboard.admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard.sites');
    }
}
