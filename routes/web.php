<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/admin', function () {
    return view('admin.admin');
});

Route::get('/', function () {
    return view('layouts.master');
});

// ROUTE ADMIN 
Route::resource('admin', 'AdminController');
Route::get('/admin', 'AdminController@index')->name('admin');
// Route::get('/admin/create', 'AdminController@create');
Route::post('/admin/store', 'AdminController@store')->name('add_admin');
// Route::get('/admin/edit/{admin}', 'AdminController@edit')->name('edit_admin');
Route::put('/admin/update/{admin}', 'AdminController@update')->name('update_admin');
Route::delete('/admin/delete/{admin}', 'AdminController@destroy')->name('delete_admin');

//ROUTE FAQ
Route::resource('faq', 'FaqController');
Route::get('/faq', 'FaqController@index')->name('faq');
Route::get('/faq/create', 'FaqController@create')->name('form_faq');
Route::post('/faq/store', 'FaqController@store')->name('add_faq');
Route::get('/faq/edit/{faq}', 'FaqController@edit')->name('edit_faq');
Route::put('/faq/update/{faq}', 'FaqController@update')->name('update_faq');
Route::delete('/faq/delete/{faq}', 'FaqController@destroy')->name('delete_faq');

//ROUTE VISION
Route::resource('vision', 'VisionController');
Route::get('/vision', 'VisionController@index')->name('vision');
// Route::get('/admin/create', 'AdminController@create');
Route::post('/vision/store', 'VisionController@store')->name('add_vision');
// Route::get('/admin/edit/{admin}', 'AdminController@edit')->name('edit_admin');
Route::put('/vision/update/{vision}', 'VisionController@update')->name('update_vision');
Route::delete('/vision/delete/{vision}', 'VisionController@destroy')->name('delete_vision');

//ROUTE MISSION
Route::resource('mission', 'MissionController');
Route::get('/mission', 'MissionController@index')->name('mission');
// Route::get('/admin/create', 'AdminController@create');
Route::post('/mission/store', 'MissionController@store')->name('add_mission');
// Route::get('/admin/edit/{admin}', 'AdminController@edit')->name('edit_admin');
Route::put('/mission/update/{mission}', 'MissionController@update')->name('update_mission');
Route::delete('/mission/delete/{mission}', 'MissionController@destroy')->name('delete_mission');

//ROUTE ADVANTAGE
Route::resource('advantage', 'AdvantageController');
Route::get('/advantage', 'AdvantageController@index')->name('advantage');
Route::get('/advantage/create', 'AdvantageController@create')->name('form_advantage');
Route::post('/advantage/store', 'AdvantageController@store')->name('add_advantage');
Route::get('/advantage/edit/{advantage}', 'AdvantageController@edit')->name('edit_advantage');
Route::put('/advantage/update/{advantage}', 'AdvantageController@update')->name('update_advantage');
Route::delete('/advantage/delete/{advantage}', 'AdvantageController@destroy')->name('delete_advantage');

//ROUTE FEATURE
Route::resource('feature', 'FeatureController');
Route::get('/feature', 'FeatureController@index')->name('feature');
Route::get('/feature/create', 'FeatureController@create')->name('form_feature');
Route::post('/feature/store', 'FeatureController@store')->name('add_feature');
Route::get('/feature/edit/{feature}', 'FeatureController@edit')->name('edit_feature');
Route::put('/feature/update/{feature}', 'FeatureController@update')->name('update_feature');
Route::delete('/feature/delete/{feature}', 'FeatureController@destroy')->name('delete_feature');

//ROUTE TESTIMONIAL
Route::resource('testimonial', 'TestimonialController');
Route::get('/testimonial', 'TestimonialController@index')->name('testimonial');
Route::get('/testimonial/create', 'TestimonialController@create')->name('form_testimonial');
Route::post('/testimonial/store', 'TestimonialController@store')->name('add_testimonial');
Route::get('/testimonial/edit/{testimonial}', 'TestimonialController@edit')->name('edit_testimonial');
Route::put('/testimonial/update/{testimonial}', 'TestimonialController@update')->name('update_testimonial');
Route::delete('/testimonial/delete/{testimonial}', 'TestimonialController@destroy')->name('delete_testimonial');

//ROUTE MESSAGES
Route::resource('message', 'MessageController');
Route::get('/message', 'MessageController@index')->name('message');
// Route::get('/admin/create', 'AdminController@create');
// Route::post('/mission/store', 'MissionController@store')->name('add_mission');
// Route::get('/admin/edit/{admin}', 'AdminController@edit')->name('edit_admin');
Route::put('/message/update/{message}', 'MessageController@update')->name('update_message');
Route::delete('/message/delete/{message}', 'MessageController@destroy')->name('delete_message');

//ROUTE SITES
Route::resource('site', 'SiteController');
// Route::get('/site', 'SiteController@index')->name('site');
// Route::get('/admin/create', 'AdminController@create');
// Route::post('/mission/store', 'MissionController@store')->name('add_mission');
// Route::get('/site/edit', 'SiteController@edit')->name('edit_site');
Route::put('/site/update/1', 'SiteController@update')->name('update_site');
// Route::delete('/message/delete/{message}', 'MessageController@destroy')->name('delete_message');

// ROUTE PRODUCT 
Route::resource('product', 'ProductController');
Route::get('/product', 'ProductController@index')->name('product');
// Route::get('/product/create', 'ProductController@create');
Route::post('/product/store', 'ProductController@store')->name('add_product');
// Route::get('/product/edit/{product}', 'ProductController@edit')->name('edit_product');
Route::put('/product/update/{product}', 'ProductController@update')->name('update_product');
Route::delete('/product/delete/{product}', 'ProductController@destroy')->name('delete_product');

// AUTH
Auth::routes(['register' => false]);

// ROUTE LOGIN
Route::prefix('admins')->group(function(){
    Route::get('/login', 'Auth\AdminAuthController@getLogin')->name('admin.login');
    Route::post('/login', 'Auth\AdminAuthController@postLogin')->name('admin.login.submit');
  });

// ROUTE LOGOUT
Route::post('/logout', 'Auth\AdminAuthController@postLogout')->name('master.submit');