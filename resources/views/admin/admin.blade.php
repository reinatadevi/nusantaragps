@extends('layouts.master')

@section('title')
    Dashboard | Admin
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Admin</h1>
    </div>
    <div class="section-body">
      <div class="row">
          <div class="col-md-12 col-lg-12">
              <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-primary my-4" data-toggle="modal" data-target="#addModal">
                      Tambah Data Admin
                    </button>
                    <div class="card-header-form ml-auto">
                      <form method="GET" action="{{ route('admin') }}">
                        <div class="input-group">
                          <input type="text" class="form-control" id="search" name="search" placeholder="Search by name">
                          <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                          </div>
                        </div>
                      </form>
                    </div>
                </div>
                  <div class="card-body">
                      <table class="table table-striped">
                          <table class="table table-hover table-striped table-bordered ">
                          <thead>
                              <tr>
                                  <th scope="col" class="text-center">No</th>
                                  <th scope="col" class="text-center">Name</th>
                                  <th scope="col" class="text-center">Username</th>
                                  <th scope="col" class="text-center">Email</th>
                                  <th scope="col" class="text-center">Updated At</th>
                                  <th scope="col" class="text-center">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($admin as $index => $adm)
                          <tr>
                              <td class="text-center">{{$index+1}}</td>
                              <td class="text-center">{{$adm->name}}</td>
                              <td class="text-center">{{$adm->username}}</td>
                              <td class="text-center">{{$adm->email}}</td>
                              <td class="text-center">{{$adm->updated_at}}</td>
                              <td class="text-center">
                                  <button class="btn btn-sm" data-name="{{$adm->name}}" data-username="{{$adm->username}}" data-email="{{$adm->email}}" data-id="{{$adm->id}}" data-toggle="modal" data-target="#editModal">
                                    <img src="{{ asset('/assets/img/icon/icon-edit.png') }}">
                                  </button>
                                  <button class="btn btn-sm" data-id="{{$adm->id}}" data-toggle="modal" data-target="#deleteModal">
                                    <img src="{{ asset('/assets/img/icon/icon-delete.png') }}">
                                  </button>
                              </td>
                          </tr>
                          @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
    </div>
</section>

<!-- Modal Add Data -->
<div class="modal fade" id="addModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addModalLabel">Tambah Data Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('add_admin') }}" enctype="multipart/form-data">
        @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="name" id="nama" placeholder="Nama" required>
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="Username" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
            </div>        
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Cancel</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Data -->
<div class="modal fade" id="editModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Data Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
        {{csrf_field()}}
        {{method_field('put')}} <!-- cek change method -->
        <input type="hidden" name="id" id="id" value="">
            <div class="form-group">
            <label for="nama">Nama</label>
                <input type="text" class="form-control" name="name" id="name" value="">
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="username" id="username" value="">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="email" id="email" value="">
            </div>      
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Cancel</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Delete Data -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Delete Data Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="delete_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
        {{csrf_field()}}
        {{method_field('delete')}}
      <div class="modal-body">
      <input type="hidden" name="id" id="id" value="">
        <p>Apakah anda yakin akan menghapus data ini?</p>
      </div>
      <div class="modal-footer">        
        <button type="submit" class="btn btn-primary">Ya</button>        
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Tidak</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
  $('#editModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var name = button.data('name')
    var username = button.data('username')
    var email = button.data('email')
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('admin.update', '') }}/${id}`; // generate url
    $('#edit_form').attr('action', url); // set edit_form action attribute  

    modal.find('.modal-body #name').val(name);
    modal.find('.modal-body #username').val(username);
    modal.find('.modal-body #email').val(email);
    modal.find('.modal-body #id').val(id);
  })

  $('#deleteModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('admin.destroy', '') }}/${id}`; // generate url
    $('#delete_form').attr('action', url); // set edit_form action attribute

    modal.find('.modal-body #id').val(id);
  })

</script>
@endsection