@extends('layouts.master')

@section('title')
    Dashboard | Product
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Data Produk</h1>
    </div>
    <div class="section-body">
      <div class="row">
          <div class="col-md-12 col-lg-12">
              <div class="card">
                  <div class="card-header">
                    <button type="button" class="btn btn-primary my-4" data-toggle="modal" data-target="#addModal">
                      Tambah Produk
                    </button>
                    <div class="card-header-form ml-auto">
                      <form method="post" action="">
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="Search">
                          <div class="input-group-btn">
                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card-body">
                      <div class="table-responsive">                    
                          <!-- <button type="button" class="btn btn-primary my-4" data-toggle="modal" data-target="#addModal">
                              Tambah Produk
                          </button> -->
                          <table class="table table-hover table-striped table-bordered ">
                          <thead>
                              <tr>
                                  <th scope="col" class="text-center">No</th>
                                  <th scope="col" class="text-center">Nama Produk</th>
                                  <!-- <th scope="col" class="text-center">Fitur</th> -->
                                  <th scope="col" class="text-center">Deskripsi</th>
                                  <th scope="col" class="text-center">Foto</th>
                                  <th scope="col" class="text-center">Updated At</th>
                                  <th scope="col" class="text-center">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($product as $key=>$prod)
                          <tr>
                              <td class="text-center">{{$key+1}}</td>
                              <td class="text-center">{{$prod->name}}</td>
                              <!-- <td class="text-center">{{$prod->fitur}}</td> -->
                              <td class="text-center">{{$prod->description}}</td>
                              <td class="text-center">
                                <?php $img_name = substr($prod->image, 13);?>
                                <img src="{{ asset('/storage/images').$img_name}}" alt="" title="" style="width: 100px; height: 100px;">
                              </td>
                              <td class="text-center">{{$prod->updated_at}}</td>
                              <td class="text-center">
                                  <button class="btn btn-sm" data-toggle="modal" data-target="#editModal" data-name="{{$prod->name}}" data-description="{{$prod->description}}" data-image="{{$prod->image}}" data-id="{{$prod->id}}">
                                    <img src="{{ asset('/assets/img/icon/icon-edit.png') }}">
                                  </button>
                                  <button class="btn btn-sm" data-id="{{$prod->id}}" data-toggle="modal" data-target="#deleteModal">
                                    <img src="{{ asset('/assets/img/icon/icon-delete.png') }}">
                                  </button>
                              </td>
                          </tr>
                          @endforeach
                          </tbody>
                      </table>
                     </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
</section>


<!-- Modal Add Data -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalLabel">Tambah Data Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('add_product') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Produk</label>
                        <input type="text" name="name" id="name" placeholder="Nama Produk" class="form-control">
                    </div>
                    <!-- <div class="form-group">
                        <label>Fitur</label>
                        <input type="text" name="fitur" id="fitur" placeholder="Fitur" class="form-control">
                    </div> -->
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea type="text" name="description" id="description" placeholder="Deskripsi" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="image" id="image" class="form-control-file">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah Data</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit Data -->
<div class="modal fade" id="editModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Data Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
        {{csrf_field()}}
        {{method_field('put')}} <!-- cek change method -->
        <input type="hidden" name="id" id="id" value="">
            <div class="form-group">
            <label for="nama">Nama</label>
                <input type="text" class="form-control" name="name" id="name" value="">
            </div>
            <div class="form-group">
                <label for="description">Deskripsi</label>
                <textarea type="text" class="form-control" name="description" id="description" value=""></textarea>
            </div>
            <div class="form-group">
                <label>Foto</label>
                <input type="file" name="image" id="image" class="form-control-file" value="">
            </div>     
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Cancel</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Delete Data -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Delete Data Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="delete_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
        {{csrf_field()}}
        {{method_field('delete')}}
      <div class="modal-body">
      <input type="hidden" name="id" id="id" value="">
        <p>Apakah anda yakin akan menghapus data ini?</p>
      </div>
      <div class="modal-footer">        
        <button type="submit" class="btn btn-primary">Ya</button>        
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Tidak</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
  $('#editModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var name = button.data('name')
    var description = button.data('description')
    var image = button.data('image')
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('product.update', '') }}/${id}`; // generate url
    $('#edit_form').attr('action', url); // set edit_form action attribute  

    modal.find('.modal-body #name').val(name);
    modal.find('.modal-body #description').val(description);
    modal.find('.modal-body #image').val(image);
    modal.find('.modal-body #id').val(id);
  })

  $('#deleteModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('product.destroy', '') }}/${id}`; // generate url
    $('#delete_form').attr('action', url); // set edit_form action attribute

    modal.find('.modal-body #id').val(id);
  })

</script>
@endsection