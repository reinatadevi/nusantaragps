@extends('layouts.master')

@section('title')
    Dashboard | Site Options
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Pengaturan Halaman Utama</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Site Options</h4>
                    </div>
                    <div class="card-body">
                    <form method="POST" action="" enctype=" multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('post') }}                                                 
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Author</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" class="form-control input-author" id="author" name="author" Placeholder="Author" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Description</label>
                            <div class="col-sm-12 col-md-7">
                                <textarea rows="6" class="form-control input-description" id="description" name="description" Placeholder="Description" value=""></textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Login URL</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" class="form-control input-url" id="url" name="url" Placeholder="Login URL" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-6 col-md-3 col-lg-3">Telepon</label>
                            <div class="col-sm-12 col-md-3">
                                <input type="number" class="form-control input-telephone" id="telephone" name="telephone" Placeholder="Telephone" value="">
                            </div>
                            <label class="col-form-label text-md-right col-6 col-md-1 col-lg-1">Email</label>
                            <div class="col-sm-12 col-md-3">
                                <input type="email" class="form-control input-email" id="email" name="email" Placeholder="Email" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alamat</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" class="form-control input-address" id="address" name="address" Placeholder="Address" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kode Pos</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" class="form-control input-postal-code" id="postal_code" name="postal_code" Placeholder="Kode Pos" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Copyright</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" class="form-control input-copyright" id="copyright" name="copyright" Placeholder="Copyright" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('scripts')

@endsection