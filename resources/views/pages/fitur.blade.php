@extends('layouts.master')

@section('title')
    Dashboard | Fitur
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Data Fitur</h1>
    </div>
    <div class="section-body">
      <div class="row">
          <div class="col-md-12 col-lg-12">
              <div class="card">
                  <div class="card-header">
                    <a href="{{ route('form_feature') }}" class="btn btn-primary my-4">
                      Tambah Fitur
                    </a>
                    <div class="card-header-form ml-auto">
                      <form method="get" action="{{ route('feature') }}">
                        <div class="input-group">
                          <input type="text" class="form-control" name="search" placeholder="Search by title">
                          <div class="input-group-btn">
                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card-body">
                      <table class="table table-hover table-striped table-bordered">                    
                          <!-- <button type="button" class="btn btn-primary my-4" data-toggle="modal" data-target="#addModal">
                              Tambah Data Keunggulan
                          </button> -->
                      <!-- <table class="table table-striped">                     -->
                          <!-- <button type="button" class="btn btn-primary my-4" data-toggle="modal" data-target="#addModal">
                              Tambah Data Keunggulan
                          </button> -->
                          <!-- <table class="table table-hover table-striped table-bordered "> -->
                          <thead>
                              <tr>
                                  <th scope="col" class="text-center">No</th>
                                  <th scope="col" class="text-center">Judul</th>
                                  <th scope="col" class="text-center">Deskripsi</th>
                                  <th scope="col" class="text-center">Icon</th>
                                  <th scope="col" class="text-center">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($feature as $key=>$feat)
                          <tr>
                              <td class="text-center">{{$key+1}}</td>
                              <td class="text-center">{{$feat->title}}</td>
                              <td class="text-center">{{$feat->description}}</td>
                              <!-- <td class="text-center"><img src="{{URL::to('/storage/app/')}}{{$feat->image}}" style="width: 100px; height: 100px;"></td> -->
                              <!-- <td class="text-center"><img src="{{ url('/storage/app/'.$feat->image) }}" alt="" title=""></td> -->
                              <td class="text-center">
                                <?php $img_name = substr($feat->image, 13);?>
                                <img src="{{ asset('/storage/images').$img_name}}" alt="" title="" style="width: 100px; height: 100px;">
                              </td>
                              <td class="text-center">
                                  <a href="{{ route('edit_feature', $feat->id) }}" class="btn btn-sm" data-id="{{$feat->id}}" data-title="{{$feat->title}}" data-description="{{$feat->description}}" data-image="{{$feat->image}}">
                                    <img src="{{ asset('/assets/img/icon/icon-edit.png') }}">
                                  </a>
                                  <button class="btn btn-sm" data-id="{{$feat->id}}" data-toggle="modal" data-target="#deleteModal">
                                    <img src="{{ asset('/assets/img/icon/icon-delete.png') }}">
                                  </button>
                              </td>
                          </tr>
                          @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
    </div>
</section>

<!-- Modal Edit Data -->
<div class="modal fade" id="editModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Data Admin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
        {{csrf_field()}}
        {{method_field('put')}} <!-- cek change method -->
        <input type="hidden" name="id" id="id" value="">
            <div class="form-group">
            <label for="content">Judul</label>
                <input type="text" class="form-control" name="title" id="title" value="">
            </div>
            <div class="form-group">
            <label for="content">Deskripsi</label>
                <input type="text" class="form-control" name="description" id="description" value="">
            </div>
            <div class="form-group">
            <label for="content">Icon</label>
                <input type="file" class="form-control" name="image" id="image" class="form-control-file" value="">
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Edit Data</button>
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Cancel</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Delete Data -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-info" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="deleteModalLabel">Konfirmasi Hapus Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="delete_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
                {{ csrf_field() }}
                {{ method_field('delete') }}
                <div class="modal-body">
                    <input type="hidden" name="id" value="" id="id">
                    <p class="text-area">
                        Apakah anda yakin akan menghapus data ini?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Iya, Hapus </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
  $('#editModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var admin_id = button.data('admin_id')
    var title = button.data('title')
    var description = button.data('description')
    var image = button.data('image')
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('feature.update', '') }}/${id}`; // generate url
    $('#edit_form').attr('action', url); // set edit_form action attribute  

    modal.find('.modal-body #admin_id').val(admin_id);
    modal.find('.modal-body #title').val(title);
    modal.find('.modal-body #description').val(description);
    modal.find('.modal-body #image').val(image);
  })

  $('#deleteModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('feature.destroy', '') }}/${id}`; // generate url
    $('#delete_form').attr('action', url); // set edit_form action attribute

    modal.find('.modal-body #id').val(id);
  })
</script>
@endsection