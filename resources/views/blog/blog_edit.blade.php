@extends('layouts.master')

@section('title')
    Dashboard | Blog Edit
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Edit Article</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Artikel</h4>
                    </div>
                    <div class="card-body">
                    @foreach($blog as $key=>$blg)
                    <form method="POST" action="" enctype=" multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Judul</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Author</label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control selectric">
                                    <option>Publish</option>
                                    <option>Draft</option>
                                    <option>Pending</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                            <div class="col-sm-12 col-md-7">
                                <textarea class="summernote-simple"></textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Cover</label>
                            <div class="col-sm-12 col-md-7">
                                <div id="image-preview" class="image-preview">
                                    <label for="image-upload" id="image-label">Choose File</label>
                                    <input type="file" name="image" id="image-upload" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tags</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" class="form-control inputtags">
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Status</label>
                            <div class="col-sm-12 col-md-7">
                                <select class="form-control selectric">
                                    <option>Publish</option>
                                    <option>Draft</option>
                                    <option>Pending</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-primary">Create Post</button>
                            </div>
                        </div>
                    </form>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection

@section('scripts')

@endsection