@extends('layouts.master')

@section('title')
    Dashboard | Blog List
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>List Article</h1>
    </div>
    <div class="section-body">
      <div class="row">
          <div class="col-md-12 col-lg-12">
              <div class="card">
                    <div class="card-header">
                        <a href="" class="btn btn-primary my-4">
                            Tulis Artikel Baru
                        </a>
                        <div class="card-header-form ml-auto">
                        <form method="post" action="">
                            <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                            </div>
                        </form>
                        </div>
                    </div>
                  <div class="card-body">
                      <table class="table table-hover table-striped table-bordered table-responsive">
                          <thead>
                              <tr>
                                  <th scope="col" class="text-center">No</th>
                                  <th scope="col" class="text-center">Judul</th>
                                  <th scope="col" class="text-center">Author</th>
                                  <th scope="col" class="text-center">Created At</th>
                                  <th scope="col" class="text-center">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($blog as $key=>$blg)
                          <tr>
                              <td class="text-center">{{$key+1}}</td>
                              <td class="text-center">{{$blg->title}}</td>
                              <td class="text-center">{{$blg->author}}</td>
                              <td class="text-center">{{$blg->created_at}}</td>
                              <td class="text-center">
                                  <a class="btn btn-sm" href="{{ route('') }}" >
                                    <img src="{{ asset('/assets/img/icon/icon-detail.png') }}">
                                  </a>
                                  <a class="btn btn-sm" href="">
                                    <img src="{{ asset('/assets/img/icon/icon-edit.png') }}">
                                  </a>
                                  <a class="btn btn-sm" data-toggle="modal" data-target="#deleteModal">
                                    <img src="{{ asset('/assets/img/icon/icon-delete.png') }}">
                                  </a>
                              </td>
                          </tr>
                          @endforeach
                          </tbody>
                      </table>
                      Halaman : {{ $pegawai->currentPage() }} <br/>
                        Jumlah Data : {{ $pegawai->total() }} <br/>
                        Data Per Halaman : {{ $pegawai->perPage() }} <br/>
                  </div>
              </div>
          </div>
      </div>
    </div>
</section>

<!-- Modal Delete Data -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-info" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="deleteModalLabel">Hapus Data Artikel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('customer.destroy', $blg->id)}}" method="POST">
                {{ method_field('delete') }}
                {{ csrf_field() }}
                
                <div class="modal-body">
                    <input type="hidden" name="id" value="{{$blg->id}}" id="id">
                    <p class="text-area">
                        Apakah anda yakin akan menghapus data ini?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Iya, Hapus</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection