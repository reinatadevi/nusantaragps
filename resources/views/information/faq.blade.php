@extends('layouts.master')

@section('title')
    Dashboard | FAQ
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Data Frequently Asked Question (FAQ)</h1>
    </div>
    <div class="section-body">
      <div class="row">
          <div class="col-md-12 col-lg-12">
              <div class="card">
                  <div class="card-header">
                    <a href="{{ route('form_faq') }}" class="btn btn-primary my-4">
                      Tambah FAQ
                    </a>
                    <!-- <button type="button" class="btn btn-primary my-4" data-toggle="modal" data-target="#addModal">
                      Tambah FAQ
                    </button> -->
                    <div class="card-header-form ml-auto">
                      <form method="get" action="{{ route('faq') }}">
                        <div class="input-group">
                          <input type="text" class="form-control" name="search" placeholder="Search by pertanyaan">
                          <div class="input-group-btn">
                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-striped table-bordered">                    
                          <!-- <button type="button" class="btn btn-primary my-4" data-toggle="modal" data-target="#addModal">
                              Tambah FAQ
                          </button> -->
                          <thead>
                              <tr>
                                  <th scope="col" class="text-center">No</th>
                                  <th scope="col" class="text-center">Pertanyaan</th>
                                  <th scope="col" class="text-center">Jawaban</th>
                                  <th scope="col" class="text-center">Updated At</th>
                                  <th scope="col" class="text-center">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($faq as $index=>$f)
                          <tr>
                              <td class="text-center">{{$index+1}}</td>
                              <td class="text-center">{{$f->title}}</td>
                              <td class="text-center">{{$f->content}}</td>
                              <td class="text-center">{{$f->updated_at}}</td>
                              <td class="text-center">
                              <div class="d-inline">
                                 <button class="btn btn-sm" data-id="{{$f->id}}" data-title="{{$f->title}}" data-content="{{$f->content}}"
                                  data-admin_id="{{$f->admin_id}}" data-toggle="modal" data-updated_at="{{$f->updated_at}}" data-target="#detailModal">
                                    <img src="{{ asset('/assets/img/icon/icon-detail.png') }}">
                                  </button>
                                  <a href="{{ route('edit_faq', $f->id) }}" class="btn btn-sm" data-title="{{$f->title}}" data-content="{{$f->content}}" data-id="{{$f->id}}">
                                    <img src="{{ asset('/assets/img/icon/icon-edit.png') }}">
                                  </a>
                                  <button class="btn btn-sm" data-id="{{$f->id}}" data-toggle="modal" data-target="#deleteModal">
                                    <img src="{{ asset('/assets/img/icon/icon-delete.png') }}">
                                  </button> 
                              </div>
                                                                   
                              </td>
                          </tr>
                          @endforeach
                          </tbody>
                      </table>
                    </div>
                      
                  </div>
              </div>
          </div>
      </div>
    </div>
</section>

<!-- Modal Detail Data -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-info" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Detail Data FAQ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="detail_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
            {{csrf_field()}}
            {{method_field('put')}} <!-- cek change method -->
                <div class="modal-body">
                    <input type="hidden" name="id" value="" id="id">
                    <div class="form-group">
                        <label>Pertanyaan</label>
                        <input type="text" name="title" id="title" value=""  class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label>Jawaban</label>
                        <textarea name="content" id="content" value="" class="form-control" readonly></textarea>
                    </div>
                    <div class="form-group">
                        <label>Updated At</label>
                        <input type="text" name="updated_at" id="updated_at" value="" class="form-control" readonly>
                    </div>
                </div>            
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit Data -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-info" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Data FAQ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">
                <form id="edit_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
                {{ csrf_field() }}
                {{ method_field('put') }}<!-- cek change method -->
                    <input type="hidden" name="id" value="" id="id">
                    <div class="form-group">
                        <label>Pertanyaan</label>
                        <input type="text" name="title" id="title" value="" placeholder="Pertanyaan"  class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Jawaban</label>
                        <input type="text" name="content" id="content" value="" placeholder="Jawaban" class="form-control">
                    </div>
                </div>            
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Edit Data</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Delete Data -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-info" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="deleteModalLabel">Konfirmasi Hapus Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="delete_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
                {{ csrf_field() }}
                {{ method_field('delete') }}
                <div class="modal-body">
                    <input type="hidden" name="id" value="" id="id">
                    <p class="text-area">
                        Apakah anda yakin akan menghapus data ini?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Iya, Hapus </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
  $('#editModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var title = button.data('title')
    var content = button.data('content')
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('faq.update', '') }}/${id}`; // generate url
    $('#edit_form').attr('action', url); // set edit_form action attribute  

    modal.find('.modal-body #title').val(title);
    modal.find('.modal-body #content').val(content);
    modal.find('.modal-body #id').val(id);
  })

  $('#deleteModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('faq.destroy', '') }}/${id}`; // generate url
    $('#delete_form').attr('action', url); // set edit_form action attribute

    modal.find('.modal-body #id').val(id);
  })

  $('#detailModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var title = button.data('title')
    var content = button.data('content')
    var admin_id = button.data('admin_id')
    var updated_at = button.data('updated_at')
    var id = button.data('id')
    var modal = $(this)

    modal.find('.modal-body #title').val(title);
    modal.find('.modal-body #content').val(content);
    modal.find('.modal-body #admin_id').val(admin_id);
    modal.find('.modal-body #updated_at').val(updated_at);
    modal.find('.modal-body #id').val(id);
  })

</script>
@endsection