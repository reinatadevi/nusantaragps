@extends('layouts.master')

@section('title')
    Dashboard | Blog FAQ
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Edit Data FAQ</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Form Edit FAQ</h4>
                    </div>
                    <div class="card-body">
                    <form method="POST" action="{{ route('update_faq', $faq->id) }}" enctype=" multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pertanyaan</label>
                            <div class="col-sm-12 col-md-7">
                                <input type="text" name="title" id="title" placeholder="Pertanyaan" value="{{$faq->title}}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jawaban</label>
                            <div class="col-sm-12 col-md-7">
                                <textarea type="text" rows="14" name="content" id="content" placeholder="Deskripsi" value="" class="form-control">{{ $faq->content }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-primary">Edit Data</button>
                                <a href="{{ route('faq') }}" class="btn btn-secondary ml-2">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection

@section('scripts')

@endsection