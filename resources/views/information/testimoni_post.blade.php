@extends('layouts.master')

@section('title')
    Dashboard | Testimoni Post
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Tambah Data Testimoni</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Form Tambah Data Testimoni</h4>
                    </div>
                    <div class="card-body">
                    <form action="{{ route('add_testimonial') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Posisi</label>
                            <div class="col-sm-12 col-md-7">
                            <input type="text" name="position" id="position" placeholder="Posisi" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Konten</label>
                            <div class="col-sm-12 col-md-7">
                            <textarea type="text" name="content" id="content" placeholder="Konten" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Foto</label>
                            <div class="col-sm-12 col-md-7">
                                <div id="image-preview" class="image-preview">
                                    <label for="image-upload" id="image-label">Choose File</label>
                                    <input type="file" name="image" id="image" class="form-control-file" accept="image/png, image/jpeg">
                                    <!-- <input type="file" name="image" id="image"> -->
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                            <div class="col-sm-12 col-md-7">
                                <button type="submit" class="btn btn-primary">Tambah Data</button>
                                <a href="{{ route('testimonial') }}" class="btn btn-secondary ml-2">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection

@section('scripts')

@endsection