@extends('layouts.master')

@section('title')
    Dashboard | Visi
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Visi</h1>
    </div>
    <div class="section-body">
      <div class="row">
          <div class="col-md-12 col-lg-12">
              <div class="card">
                  <div class="card-header">
                    <button type="button" class="btn btn-primary my-4" data-toggle="modal" data-target="#addModal">
                      Tambah Visi
                    </button>
                    <div class="card-header-form ml-auto">
                      <form method="get" action="{{ route('vision') }}">
                        <div class="input-group">
                          <input type="text" class="form-control" name="search" placeholder="Search">
                          <div class="input-group-btn">
                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card-body">
                      <table class="table table-striped">
                          <table class="table table-hover table-striped table-bordered ">
                          <thead>
                              <tr>
                                  <th scope="col" class="text-center">No</th>
                                  <th scope="col" class="text-center">Visi</th>
                                  <th scope="col" class="text-center">Updated At</th>
                                  <th scope="col" class="text-center">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($vision as $index => $visi)
                          <tr>
                              <td class="text-center">{{$index+1}}</td>
                              <td class="text-left">{{$visi->content}}</td>
                              <td class="text-center">{{$visi->updated_at}}</td>
                              <td class="text-center">
                                  <button class="btn btn-sm" data-admin_id="{{$visi->admin_id}}"  data-content="{{$visi->content}}" data-id="{{$visi->id}}" data-toggle="modal" data-target="#editModal">
                                    <img src="{{ asset('/assets/img/icon/icon-edit.png') }}">
                                  </button>
                                  <button class="btn btn-sm" data-id="{{$visi->id}}" data-toggle="modal" data-target="#deleteModal">
                                    <img src="{{ asset('/assets/img/icon/icon-delete.png') }}">
                                  </button>
                              </td>
                          </tr>
                          @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
    </div>
</section>

<!-- Modal Add Data -->
<div class="modal fade" id="addModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addModalLabel">Tambah Visi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('add_vision') }}" enctype="multipart/form-data">
        @csrf
            <div class="form-group">
                <label for="content">Visi</label>
                <input type="text" class="form-control" name="content" id="content" placeholder="Visi">
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Tambah Data</button>
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Cancel</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Data -->
<div class="modal fade" id="editModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Edit Visi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="edit_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
        {{csrf_field()}}
        {{method_field('put')}} <!-- cek change method -->
        <input type="hidden" name="id" id="id" value="">
            <div class="form-group">
            <label for="content">Visi</label>
                <input type="text" class="form-control" name="content" id="content" value="">
            </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Edit Data</button>
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Cancel</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Delete Data -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Hapus Visi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="delete_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
        {{csrf_field()}}
        {{method_field('delete')}}
      <div class="modal-body">
      <input type="hidden" name="id" id="id" value="">
        <p>Apakah anda yakin akan menghapus data ini?</p>
      </div>
      <div class="modal-footer">        
        <button type="submit" class="btn btn-primary">Ya</button>        
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Tidak</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
  $('#editModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var admin_id = button.data('admin_id')
    var content = button.data('content')
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('vision.update', '') }}/${id}`; // generate url
    $('#edit_form').attr('action', url); // set edit_form action attribute  

    modal.find('.modal-body #admin_id').val(admin_id);
    modal.find('.modal-body #content').val(content);
  })

  $('#deleteModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('vision.destroy', '') }}/${id}`; // generate url
    $('#delete_form').attr('action', url); // set edit_form action attribute

    modal.find('.modal-body #id').val(id);
  })
</script>
@endsection