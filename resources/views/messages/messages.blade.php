@extends('layouts.master')

@section('title')
    Dashboard | Messages
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Pesan Pelanggan</h1>
    </div>
    <div class="section-body">
      <div class="row">
          <div class="col-md-12 col-lg-12">
              <div class="card">
                  <div class="card-header">
                    <div class="card-header-form ml-auto">
                      <form method="GET" action="{{ route('message') }}">
                        <div class="input-group">
                          <input type="text" class="form-control" id="search" name="search" placeholder="Search by name">
                          <div class="input-group-btn">
                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="card-body">
                  <div class="table-responsive">
                      <table class="table table-hover table-striped table-bordered">
                          <thead>
                              <tr>
                                  <th scope="col" class="text-center">No</th>
                                  <th scope="col" class="text-center">Nama</th>
                                  <th scope="col" class="text-center">No. Telepon</th>
                                  <th scope="col" class="text-center">Email</th>
                                  <th scope="col" class="text-center">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                          @foreach($message as $index => $msg)
                          <tr>
                              <td class="text-center">{{$index+1}}</td>
                              <td class="text-center">{{$msg->name}}</td>
                              <td class="text-center">{{$msg->telephone}}</td>
                              <td class="text-center">{{$msg->email}}</td>
                              <td class="text-center">
                                  <button class="btn btn-sm" data-name="{{$msg->name}}" data-telephone="{{$msg->telephone}}" data-email="{{$msg->email}}" data-content="{{$msg->content}}" data-id="{{$msg->id}}" data-toggle="modal" data-target="#detailModal">
                                    <img src="{{ asset('/assets/img/icon/icon-detail.png') }}">
                                  </button>
                                  <button class="btn btn-sm" data-id="{{$msg->id}}" data-toggle="modal" data-target="#deleteModal">
                                    <img src="{{ asset('/assets/img/icon/icon-delete.png') }}">
                                  </button>
                              </td>
                          </tr>
                          @endforeach
                          </tbody>
                      </table>
                  </div>
                      
                  </div>
              </div>
          </div>
      </div>
    </div>
</section>

<!-- Detail Data -->
<div class="modal fade" id="detailModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Detail Pesan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="detail_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
        {{csrf_field()}}
        {{method_field('put')}} <!-- cek change method -->
        <input type="hidden" name="id" id="id" value="">
            <div class="form-group">
                <label for="name">Nama Pelanggan</label>
                <input type="text" class="form-control" name="name" id="name" value="" readonly>
            </div>
            <!-- <div class="form-group">
            <label for="telephone">No. Telepon</label>
                <input type="text" class="form-control" name="telephone" id="telephone" value="" readonly>
            </div>
            <div class="form-group">
            <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email" value="" readonly>
            </div> -->
            <div class="form-group">
            <label for="content">Pesan</label>
                <textarea class="form-control" name="content" id="content" value="" readonly></textarea>
            </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="submit" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Delete Data -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Hapus Pesan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="delete_form" method="POST" action=""> <!-- set action attribte to empty, the url from js -->
        {{csrf_field()}}
        {{method_field('delete')}}
      <div class="modal-body">
      <input type="hidden" name="id" id="id" value="">
        <p>Apakah anda yakin akan menghapus data ini?</p>
      </div>
      <div class="modal-footer">        
        <button type="submit" class="btn btn-primary">Ya</button>        
        <button class="btn btn-secondary" type="submit" data-dismiss="modal">Tidak</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script>
  $('#detailModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var name = button.data('name')
    var telephone = button.data('telephone')
    var email = button.data('email')
    var content = button.data('content')
    var id = button.data('id')
    var modal = $(this)

    modal.find('.modal-body #name').val(name);
    modal.find('.modal-body #telephone').val(telephone);
    modal.find('.modal-body #email').val(email);
    modal.find('.modal-body #content').val(content);
    modal.find('.modal-body #id').val(id);
  })

  $('#deleteModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget)
    var id = button.data('id')
    var modal = $(this)

    let url = `{{ route('message.destroy', '') }}/${id}`; // generate url
    $('#delete_form').attr('action', url); // set edit_form action attribute

    modal.find('.modal-body #id').val(id);
  })
</script>
@endsection

