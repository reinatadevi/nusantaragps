<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $table = 'dashboard.sites';

    protected $fillable = [
        'url', 'email', 'telephone', 'address', 'postal_code', 'description', 'author', 'copyright', 'title', 'admin_id',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }
}
