<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use softDeletes;

    protected $table = 'dashboard.blogs';

    protected $fillable = [
        'title', 'image', 'content', 'admin_id'
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function blog_fus()
    {
        return $this->hasMany('App\Models\Blog_Fu');
    }
}
