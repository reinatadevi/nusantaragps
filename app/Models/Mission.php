<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mission extends Model
{
    protected $table = 'dashboard.missions';

    protected $fillable = [
        'content', 'admin_id',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }
}
