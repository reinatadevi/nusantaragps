<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog_Fu extends Model
{
    use softDeletes;

    protected $table = 'dashboard.blog_fus';

    protected $fillable = [
        'name', 'file', 'blog_id',
    ];

    public function blog()
    {
        return $this->belongsTo('App\Models\Blog');
    }
}
