<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feature_Fu extends Model
{
    use softDeletes;

    protected $table = 'dashboard.feature_fus';

    protected $fillable = [
        'name', 'file', 'feature_id',
    ];

    public function feature()
    {
        return $this->belongsTo('App\Models\Feature');
    }
}
