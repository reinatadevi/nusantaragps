<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use softDeletes;

    protected $table = 'dashboard.products';

    protected $fillable = [
        'name', 'description', 'image', 'admin_id',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function product_fus()
    {
        return $this->hasMany('App\Models\Product_Fu');
    }

    public function product_features()
    {
        return $this->belongsToMany('App\Models\Product_Feature');
    }
}
