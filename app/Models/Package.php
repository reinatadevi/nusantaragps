<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use softDeletes;

    protected $table = 'dashboard.packages';

    protected $fillable = [
        'title', 'price', 'feature', 'discount_id',
    ];

    public function discount()
    {
        return $this->belongsTo('App\Models\Discount');
    }
}
