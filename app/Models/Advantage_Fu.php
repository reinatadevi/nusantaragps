<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advantage_Fu extends Model
{
    use SoftDeletes;

    protected $table = 'dashboard.advantage_fus';

    protected $fillable = [
        'name', 'file', 'advantage_id',
    ];

    public function advantage()
    {
        return $this->belongsTo('App\Models\Advantage');
    }
}
