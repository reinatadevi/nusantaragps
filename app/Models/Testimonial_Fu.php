<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimonial_Fu extends Model
{
    use softDeletes;

    protected $table = 'dashboard.testimonial_fus';

    protected $fillable = [
        'name', 'file', 'testimonial_id',
    ];

    public function testimonial()
    {
        return $this->belongsTo('App\Models\Testimonial');
    }
}
