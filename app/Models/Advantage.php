<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Advantage extends Model
{
    use softDeletes;

    protected $table = 'dashboard.advantages';

    protected $fillable = [
        'title', 'description', 'image', 'admin_id'
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function advantage_fus()
    {
        return $this->hasMany('App\Models\Advantage_Fu');
    }
}
