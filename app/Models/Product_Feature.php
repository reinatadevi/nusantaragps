<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Feature extends Model
{
    protected $table = 'dashboard.product_features';

    protected $fillable = [
        'description',
    ];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }
}
