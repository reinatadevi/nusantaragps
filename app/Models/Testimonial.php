<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimonial extends Model
{
    use softDeletes;

    protected $table = 'dashboard.testimonials';

    protected $fillable = [
        'position', 'image', 'content', 'admin_id',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function testimonial_fus()
    {
        return $this->hasMany('App\Models\Testimonial_Fu');
    }
}
