<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File_Upload extends Model
{
    use SoftDeletes;

    protected $table = 'dashboard.files_uploads';

    protected $fillable = [
        'name', 'file', 'admin_id',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }
}
