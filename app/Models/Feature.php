<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feature extends Model
{
    use softDeletes;

    protected $table = 'dashboard.features';

    protected $fillable = [
        'title', 'description', 'image', 'admin_id'
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function feature_fus()
    {
        return $this->hasMany('App\Models\Feature_Fu');
    }
}
