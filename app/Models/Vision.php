<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vision extends Model
{
    protected $table = 'dashboard.visions';

    protected $fillable = [
        'content', 'admin_id',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }
}
