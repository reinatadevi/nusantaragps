<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table = 'dashboard.discounts';

    protected $fillable = [
        'title', 'content', 'roulette', 'admin_id',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function packages()
    {
        return $this->hasMany('App\Models\Package');
    }
}
