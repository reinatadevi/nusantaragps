<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Authenticatable
{
    use SoftDeletes, Notifiable;
    
    protected $guard = 'admin';

    protected $table = 'dashboard.admins';
    
    protected $fillable = [
        'username', 'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function blogs()
    {
        return $this->hasMany('App\Models\Blog');
    }

    public function testimonials()
    {
        return $this->hasMany('App\Models\Testimonial');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function file_uploads()
    {
        return $this->hasMany('App\Models\File_Upload');
    }

    public function features()
    {
        return $this->hasMany('App\Models\Feature');
    }

    public function advantages()
    {
        return $this->hasMany('App\Models\Advantage');
    }

    public function missions()
    {
        return $this->hasMany('App\Models\Mission');
    }

    public function visions()
    {
        return $this->hasMany('App\Models\Vision');
    }

    public function sites()
    {
        return $this->hasMany('App\Models\Site');
    }

    public function faqs()
    {
        return $this->hasMany('App\Models\Faq');
    }

    public function discounts()
    {
        return $this->hasMany('App\Models\Discount');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }
}
