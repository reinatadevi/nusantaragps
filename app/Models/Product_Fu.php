<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Fu extends Model
{
    use softDeletes;

    protected $table = 'dashboard.product_fus';

    protected $fillable = [
        'name', 'file', 'product_id',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
