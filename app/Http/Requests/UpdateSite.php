<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSite extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'admin_id'      => 'required|string',
            'url'           => 'required|string',
            'email'         => 'required|string',
            'telephone'     => 'required|string',
            'address'       => 'required|string',
            'postal_code'   => 'required|string',
            'description'   => 'required|string',
            'author'        => 'required|string',
            'copyright'     => 'required|string',
            'title'         => 'required|string'
        ];
    }
}
