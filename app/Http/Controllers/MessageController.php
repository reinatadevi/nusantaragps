<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMessage;
use App\Http\Requests\UpdateMessage;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gate;
use Hash;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
     public function index(Request $request)
    {
        if($request->has('search')){
            $messages = Message::where('name','LIKE','%'.$request->search.'%')->get();
        } else {
            $messages = Message::all();
        }
        
        return view('messages.messages', ['message' => $messages]);
    }

    public function create()
    {
        return view('add_message');
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->only('name', 'telephone', 'email', 'content');

        $message = Message::create($data);
        return back();
    }

    public function edit(UpdateMessage $request, Message $message)
    {
        // return view('update_message', compact('messages'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Message $message)
    {
        // $message->update($request->all());
        // return back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        // dd($request);
        $message = Message::findOrFail($request->id);
        $message->delete();
        return back();
    }
}
