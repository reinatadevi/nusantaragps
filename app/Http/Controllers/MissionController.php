<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMission;
use App\Http\Requests\UpdateMission;
use App\Models\Mission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Gate;
use Hash;

class MissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    } 
    
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $missions = Mission::all();
        return view('profile.mission', ['mission' => $missions]);
    }

    public function create()
    {
        return view('add_mission');
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {   
        $data = $request->only('admin_id', 'content');
        $data['admin_id'] = Auth::id();

        $mission = Mission::create($data);
        return back();
    }

    public function edit(UpdateMission $request, Mission $mission)
    {
        return view('update_mission', compact('missions'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Mission $mission)
    {
        $mission->update($request->all());
        return back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        // dd($request);
        $mission = Mission::findOrFail($request->id);
        $mission->delete();
        return back();
    }
}
