<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAdmin;
use App\Http\Requests\UpdateAdmin;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gate;
use Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        if($request->has('search')){
            $admins = Admin::where('name','LIKE','%'.$request->search.'%')->get();
        } else {
            $admins = Admin::all();
        }
        
        return view('admin.admin', ['admin' => $admins]);
    }

    public function create()
    {
        return view('add_admin');
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->only('username', 'name', 'email');
        $data['password'] = Hash::make($request->password);

        $admin = Admin::create($data);
        return back();
    }

    public function edit(UpdateAdmin $request, Admin $admin)
    {
        return view('update_admin', compact('admins'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Admin $admin)
    {
        $admin->update($request->all());
        return back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        // dd($request);
        $admin = Admin::findOrFail($request->id);
        $admin->delete();
        return back();
    }

    // public function search(Request $request)
	// {
	// 	// menangkap data pencarian
	// 	$search = $request->search;
 
    // 	// mengambil data dari table pegawai sesuai pencarian data
	// 	$admin = DB::table('admins')->where('name','like',"%".$search."%")->get();
 
    // 	// mengirim data pegawai ke view index
    //     return view('admin.admin', ['admins' => $admin]);
 
    // }
    // public function search(Request $request)
    // {
    //     $search = $request->get('search');
    //     $hasil = Admin::where('name', 'LIKE', '%' . $search . '%')->get();

    //     return view('admin.admin', compact('hasil', 'query'));
    // }
}
