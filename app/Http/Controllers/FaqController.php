<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFaq;
use App\Http\Requests\UpdateFaq;
use App\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Gate;
use Hash;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        if($request->has('search')){
            $faqs = Faq::where('title','LIKE','%'.$request->search.'%')->get();
        } else {
            $faqs = Faq::all();
        }
        
        return view('information.faq', ['faq' => $faqs]);
    }

    public function create()
    {
        return view('information.faq_form', compact('faqs'));
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->only('title', 'content', 'admin_id');
        $data['admin_id'] = Auth::id();

        $faq = Faq::create($data);
        return redirect()->route('faq');
    }

    public function edit(Faq $faq)
    {
        return view('information.faq_edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Faq $faq)
    {
        //dd($request);
        $faq->update($request->all());
        return redirect()->route('faq')->with('status', 'Data FAQ Berhasil Diubah!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        // dd($request);
        $faq = Faq::findOrFail($request->id);
        $faq->delete();
        return back()->with('status', 'Data FAQ Berhasil Dihapus!');
    }
}
