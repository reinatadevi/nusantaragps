<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVision;
use App\Http\Requests\UpdateVision;
use App\Models\Vision;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Gate;
use Hash;

class VisionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if($request->has('search')){
            $visions = Vision::where('content','LIKE','%'.$request->search.'%')->get();
        } else {
            $visions = Vision::all();
        }
        
        return view('profile.vision', ['vision' => $visions]);
    }

    public function create()
    {
        return view('add_vision');
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->only('admin_id', 'content');
        $data['admin_id'] = Auth::id();

        $vision = Vision::create($data);
        return back();
    }

    public function edit(UpdateVision $request, Vision $vision)
    {
        return view('update_vision', compact('admins'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Vision $vision)
    {
        $vision->update($request->all());
        return back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        // dd($request);
        $vision = Vision::findOrFail($request->id);
        $vision->delete();
        return back();
    }
}
