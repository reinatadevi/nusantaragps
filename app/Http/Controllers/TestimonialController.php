<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTestimonial;
use App\Http\Requests\UpdateTestimonial;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Gate;

class TestimonialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    } 

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if($request->has('search')){
            $testimonial = Testimonial::where('title','LIKE','%'.$request->search.'%')->get();
        } else {
            $testimonial = Testimonial::all();
        }
        
        return view('information.testimoni', ['testimonial' => $testimonial]);
    }    

    public function create()
    {
        return view('information.testimoni_post', compact('testimonial'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {   
        //dd($request);
        $data = $request->only('admin_id', 'position', 'content', 'image');
        $data['admin_id'] = Auth::id();

        if ($request->has('image')){
            $path = $request->file('image')->store('public/images');
            $data['image'] = $path;
        }

        $testimonial = Testimonial::create($data);
        return redirect()->route('testimonial');
    }

    public function edit(Testimonial $testimonial)
    {
        return view('information.testimoni_edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Testimonial $testimonial)
    {
        $data = $request->only('admin_id', 'position', 'content', 'image');
        $data['admin_id'] = Auth::id();

        if ($request->has('image')){
            Storage::delete($testimonial->image);
            $path = $request->file('image')->store('public/images');
            $data['image'] = $path;
        }

        $testimonial->update($data);
        return redirect()->route('testimonial');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        // dd($request);
        $testimonial = Testimonial::findOrFail($request->id);
        $testimonial->delete();
        return back();
    }
}
