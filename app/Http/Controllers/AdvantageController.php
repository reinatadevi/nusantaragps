<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAdvantage;
use App\Http\Requests\UpdateAdvantage;
use App\Models\Advantage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Gate;

class AdvantageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    } 
    
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if($request->has('search')){
            $advantages = Advantage::where('title','LIKE','%'.$request->search.'%')->get();
        } else {
            $advantages = Advantage::all();
        }
        
        return view('pages.keunggulan', ['advantage' => $advantages]);
    }

    public function create()
    {
        return view('pages.keunggulan_post', compact('advantages'));
    }
    
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {   
        //dd($request);
        $data = $request->only('admin_id', 'title', 'description', 'image');
        $data['admin_id'] = Auth::id();

        if ($request->has('image')){
            $path = $request->file('image')->store('public/images');
            $data['image'] = $path;
        }

        $advantage = Advantage::create($data);
        return redirect()->route('advantage');
    }

    public function edit(UpdateAdvantage $request, Advantage $advantage)
    {
        return view('pages.keunggulan_edit', compact('advantages'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Advantage $advantage)
    {
        $data = $request->only('admin_id', 'title', 'description', 'image');
        $data['admin_id'] = Auth::id();

        if ($request->has('image')){
            Storage::delete($advantage->image);
            $path = $request->file('image')->store('public/images');
            $data['image'] = $path;
        }

        $advantage->update($data);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        // dd($request);
        $advantage = Advantage::findOrFail($request->id);
        $advantage->delete();
        return back();
    }
}
