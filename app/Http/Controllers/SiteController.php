<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateSite;
use App\Models\Site;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Gate;
use Hash;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $sites = Site::all();
        return view('sites.sites', ['site' => $sites]);
    }

    public function create()
    {
        return view('add_site');
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->only('admin_id', 'content');
        $data['admin_id'] = Auth::id();

        $site = Site::create($data);
        return back();
    }

    public function edit(UpdateSite $request, Site $site)
    {
        // return view('update_site', compact('sites'));
        // $site = DB::table('site')->where('id', '1')->get();
        return view('edit',['site' => $site]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Site $site)
    {
        // dd($request);
        $site->where('id', '1')->update($request->only(['url', 'email', 'telephone', 'address', 'postal_code', 'description', 'author', 'copyright', 'title', 'admin_id']));
        // DB::table('site')->where('id', '1')->update($request->all());
        return back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        // dd($request);
        $site = Site::findOrFail($request->id);
        $site->delete();
        return back();
    }
}
