<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFeature;
use App\Http\Requests\UpdateFeature;
use App\Models\Feature;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Gate;

class FeatureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    } 

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if($request->has('search')){
            $feature = Feature::where('title','LIKE','%'.$request->search.'%')->get();
        } else {
            $feature = Feature::all();
        }
        
        return view('pages.fitur', ['feature' => $feature]);
    }

    public function create()
    {
        return view('pages.fitur_post', compact('feature'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {   
        //dd($request);
        $data = $request->only('admin_id', 'title', 'description', 'image');
        $data['admin_id'] = Auth::id();

        if ($request->has('image')){
            $path = $request->file('image')->store('public/images');
            $data['image'] = $path;
        }

        $feature = Feature::create($data);
        return redirect()->route('feature');
    }

    public function edit(Feature $feature)
    {
        return view('pages.fitur_edit', compact('feature'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Feature $feature)
    {
        $data = $request->only('admin_id', 'title', 'description', 'image');
        $data['admin_id'] = Auth::id();

        if ($request->has('image')){
            Storage::delete($feature->image);
            $path = $request->file('image')->store('public/images');
            $data['image'] = $path;
        }

        $feature->update($data);
        return redirect()->route('feature');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        // dd($request);
        $feature = Feature::findOrFail($request->id);
        $feature->delete();
        return back();
    }
}
