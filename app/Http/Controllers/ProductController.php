<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProduct;
use App\Http\Requests\UpdateProduct;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Gate;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $products = Product::all();
        return view('product.product', ['product' => $products]);
    }

    public function create()
    {
        return view('add_product');
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->only('admin_id', 'name', 'description', 'image');
        $data['admin_id'] = Auth::id();

        if ($request->has('image')){
            $path = $request->file('image')->store('public/images');
            $data['image'] = $path;
        }

        $product = Product::create($data);
        return redirect()->route('product');
    }

    public function edit(UpdateProduct $request, Product $product)
    {
        return view('update_product', compact('products'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {
        $data = $request->only('admin_id', 'name', 'description', 'image');
        $data['admin_id'] = Auth::id();

        if ($request->has('image')){
            Storage::delete($product->image);
            $path = $request->file('image')->store('public/images');
            $data['image'] = $path;
        }

        $product->update($data);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        // dd($request);
        $product = Product::findOrFail($request->id);
        $product->delete();
        return back();
    }
}
